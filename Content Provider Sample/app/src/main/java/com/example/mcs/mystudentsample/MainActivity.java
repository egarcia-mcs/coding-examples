package com.example.mcs.mystudentsample;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button saveConsultant = (Button) findViewById(R.id.button_add_student);
        final Button getConsultant = (Button) findViewById(R.id.button_get_student);
        final EditText nameEditText = (EditText) findViewById(R.id.text_name);
        final EditText gradeEditText = (EditText) findViewById(R.id.text_grade);

        saveConsultant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Save using the content provider
                saveConsultant(nameEditText.getText().toString(), gradeEditText.getText().toString());
            }
        });
        getConsultant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Retrieve results from the content provider
            }
        });

    }

    private void saveConsultant(String name, String grade) {
        ContentValues values = new ContentValues();
        values.put(ConsultantTable.COLUMN_NAME, name);
        values.put(ConsultantTable.COLUMN_GRADE, grade);

        // New Consultant
        Uri consultantUri = getContentResolver().insert(MCSContentProvider.CONTENT_URI, values);
        if ( consultantUri != null){
            Log.d(this.getClass().getSimpleName(), "Inserted: " + consultantUri.toString() );
            Toast.makeText(this, "Inserted: " + consultantUri.toString(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Unable to save information", Toast.LENGTH_SHORT).show();
        }

    }

    private void getConsultant(String name){

        Uri uri = MCSContentProvider.CONTENT_URI;
        String[] projection = new String [] {
                ConsultantTable.COLUMN_ID,
                ConsultantTable.COLUMN_NAME,
                ConsultantTable.COLUMN_GRADE
        };

        String selectionClause = ConsultantTable.COLUMN_NAME + " = ?";
        String[] selectionArgs = { name };
        Cursor cursor = getContentResolver().query(
                MCSContentProvider.CONTENT_URI,
                projection,
                selectionClause,
                selectionArgs,
                null
                );
    }

}










