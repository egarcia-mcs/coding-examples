package com.example.mcs.mystudentsample;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ConsultantDatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "consultant_table.db";
    private static final int DATABASE_VERSION = 1;

    public ConsultantDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        ConsultantTable.onCreate(database);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        ConsultantTable.onUpgrade(database, oldVersion, newVersion);
    }
}
