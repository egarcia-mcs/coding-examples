package com.example.mcs.mystudentsample;


import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ConsultantTable {
    // One class per table is a recommended practice.
    public static final String TABLE_NAME = "Consultants";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_GRADE = "grade";

    public static final String CREATE_CONSULTANT_TABLE =
            "CREATE TABLE " + TABLE_NAME
                    + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_NAME + " TEXT NOT NULL, "
                    + COLUMN_GRADE + " INTEGER DEFAULT 0"
                    + ");";

    public static void onCreate(SQLiteDatabase database){
        database.execSQL(CREATE_CONSULTANT_TABLE);
    }

    public static void  onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.d(ConsultantTable.class.getSimpleName(), "Upgrading database from " + oldVersion + " to " + newVersion + ", destroying local data.");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
