package com.example.mcs.mystudentsample;


import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.HashSet;

public class MCSContentProvider extends ContentProvider {

    private SQLiteDatabase database;
    private static final UriMatcher sUriMarcher;
    // Urimatcher IDS
    static final int CONSULTANTS = 10;
    static final int CONSULTANT_ID = 20;
    static final String RESOURCE = "consultants";
    static final String AUTHORITY = "com.example.mcs.mystudentsample.MCSContentProvider";
    static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + RESOURCE);

    static{
        sUriMarcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMarcher.addURI(AUTHORITY, RESOURCE, CONSULTANTS);
        sUriMarcher.addURI(AUTHORITY, RESOURCE + "/#", CONSULTANT_ID);
    }




    @Override
    public boolean onCreate() {
        ConsultantDatabaseHelper databaseHelper = new ConsultantDatabaseHelper(getContext());
        database = databaseHelper.getWritableDatabase();
        return database != null; // Return true if the provider was loaded successfully
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        checkColumns(projection);
        queryBuilder.setTables(ConsultantTable.TABLE_NAME);
        int uriType = sUriMarcher.match(uri);
        switch(uriType){
            case CONSULTANTS:
                break;
            case CONSULTANT_ID:
                queryBuilder.appendWhere(ConsultantTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        Cursor cursor = queryBuilder.query(database, projection, selection, selectionArgs, null, null, sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int uriType = sUriMarcher.match(uri);
        long id = 0;
        switch (uriType) {
            case CONSULTANTS:
                id = database.insert(ConsultantTable.TABLE_NAME, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(RESOURCE + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    private void checkColumns(String[] projection) {
        String[] available = {
                ConsultantTable.COLUMN_ID,
                ConsultantTable.COLUMN_NAME,
                ConsultantTable.COLUMN_GRADE
        };

        if (projection != null){
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));

            // Check if all the columns requested are available
            // If not the availableColumns contains all the strings contained in requested Columns
            if(!availableColumns.containsAll(requestedColumns)){
                // throw an illegal argument exception
                throw new IllegalArgumentException("Unknown columns in projection");
            }

        }
    }
}
