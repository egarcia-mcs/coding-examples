package com.example.mcs.recyclerviewsample;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class TodoRecyclerViewAdapter extends RecyclerView.Adapter {

    private final List<TodoDummyContent.DummyTodoItem> mValues;
    public TodoRecyclerViewAdapter(List <TodoDummyContent.DummyTodoItem> items){
        mValues = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (TodoRecyclerViewAdapter.ViewHolder) holder;
        TodoDummyContent.DummyTodoItem item = mValues.get(position);
        viewHolder.mItem = item;
        viewHolder.mIdView.setText(item.id);
        viewHolder.mHeaderView.setText(item.header);
        viewHolder.mView.setTag(position);
        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(this.getClass().getSimpleName(), "Child item clicked: " + v.getTag() );
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public final View mView;
        public final TextView mIdView;
        public final TextView mHeaderView;
        public TodoDummyContent.DummyTodoItem mItem;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mIdView = (TextView) itemView.findViewById(R.id.id);
            mHeaderView =(TextView) itemView.findViewById(R.id.header);

        }
    }
}
