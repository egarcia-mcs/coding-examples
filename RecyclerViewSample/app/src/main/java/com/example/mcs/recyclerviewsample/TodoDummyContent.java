package com.example.mcs.recyclerviewsample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TodoDummyContent {
    public static final List<DummyTodoItem> ITEMS = new ArrayList<DummyTodoItem>();
    public static final Map<String, DummyTodoItem> ITEM_MAP = new HashMap<String, DummyTodoItem>();

    static {
        addItem(new DummyTodoItem("0", "Buy Milk"));
        addItem(new DummyTodoItem("1", "SEND DAILY REPORT"));
        addItem(new DummyTodoItem("2", "Buy Belt"));
        addItem(new DummyTodoItem("3", "Upload assignment"));
    }

    public static void addItem(DummyTodoItem item){
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    public static class DummyTodoItem {
        public final String id;
        public final String header;

        public DummyTodoItem(String id, String header){
            this.id = id;
            this.header = header;
        }
    }
}

