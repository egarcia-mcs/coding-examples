package com.example.egarcia.simplelogin.utils;

import org.json.JSONObject;

public interface NetworkingHandlerInterface {
    void onResponse(JSONObject response);
    void onErrorResponse(String error);
}
