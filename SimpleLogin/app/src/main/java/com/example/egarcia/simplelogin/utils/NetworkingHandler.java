package com.example.egarcia.simplelogin.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.egarcia.simplelogin.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class NetworkingHandler {
    private final String baseAddress = "https://api.talech.com";
    private final String clientVersion = "4.2.18.0";
    private Context mContext;
    private RequestQueue requestQueue;

    public NetworkingHandler(Context context) {
        if(context instanceof NetworkingHandlerInterface){
            mContext = context;
            requestQueue = Volley.newRequestQueue(mContext);
        } else {
            throw new IllegalArgumentException("The context object has to implement the Networking handler interface");
        }
    }

    public void merchantLogin(String user, String password){
        String merchantsURI = "/authentication/signin/merchants";

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String errorMessage = "Error during merchantLogin";
                if (volleyError.networkResponse == null) {
                    if (volleyError.getClass().equals(TimeoutError.class)) {
                        // Timeout error message
                        errorMessage = "No connection to the service";
                    } else {
                        errorMessage = mContext.getResources().getString(R.string.all_no_network);
                    }
                }
                Log.d(this.getClass().getSimpleName(), errorMessage);
                ((NetworkingHandlerInterface) mContext).onErrorResponse(errorMessage);
            }
        };

        Response.Listener responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                String errorMessage = mContext.getResources()
                        .getString(R.string.networking_handler_response_error); // Generic error
                if (response != null) {
                    try {
                        JSONObject responseCode = response.getJSONObject("ResponseCode");
                        if(responseCode.getInt("statusCode") != 200){
                            errorMessage = responseCode.getString("desc"); // Get server message
                        } else {
                            // Success
                            ((NetworkingHandlerInterface) mContext).onResponse(response);
                        }
                    } catch (JSONException jsonException){
                        // TODO: custom error messages based on the responseCode
                        Log.d(this.getClass().getSimpleName(), jsonException.toString());
                    }
                }
                ((NetworkingHandlerInterface) mContext).onErrorResponse(errorMessage);
            }
        };

        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("clientVersion", clientVersion);
            jsonData.put("email", user);
            jsonData.put("password", password);
        } catch (JSONException e) {
            Log.d(this.getClass().getSimpleName(), "Error while creating the JSON object");
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                baseAddress + merchantsURI,
                jsonData,
                responseListener, errorListener) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                TimeZone timeZone = TimeZone.getDefault();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale.getDefault());
                String currentTime = simpleDateFormat.format(new Date());

                headers.put("RequestTimeStamp",currentTime); // "05/23/2017 11:19:12"
                headers.put("TimeZone",timeZone.getID());
                headers.put("clientVersion", clientVersion);

                //headers.put("securityToken", "107566534/E5GsXw9qGbAIOCGkvvCsx0Mr");
                headers.put("DeviceId","5A2A32BF-E60E-4417-AF19-5EDC0A09FEAD"); // TODO: Generate a unique device ID
                return headers;
            }
        };
        request.setTag(this.getClass().getSimpleName());
        requestQueue.add(request);
    }

    public void getSessionToken(String merchantToken){
        String getSecurityTokenURI = "/authentication/getSecurityToken";
        Response.Listener responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Retrieve session token
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Failed
            }
        };
        JSONObject jsonData = new JSONObject();
        // TODO: Insert merchant key
        JsonRequest jsonRequest = new JsonObjectRequest(Request.Method.POST,
                baseAddress + getSecurityTokenURI,
                jsonData,
                responseListener,
                errorListener);
    }

    public void cancel(){
        requestQueue.cancelAll(this.getClass().getSimpleName());
    }
}
