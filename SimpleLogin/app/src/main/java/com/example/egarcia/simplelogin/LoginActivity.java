package com.example.egarcia.simplelogin;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.egarcia.simplelogin.utils.NetworkingHandler;
import com.example.egarcia.simplelogin.utils.NetworkingHandlerInterface;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity implements NetworkingHandlerInterface {
    private LinearLayoutCompat linearLayoutCompat;
    private ProgressBar progressBar;
    private NetworkingHandler networkingHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        networkingHandler = new NetworkingHandler(this);

        Button loginButton = (Button) findViewById(R.id.button_login_submit);
        linearLayoutCompat = (LinearLayoutCompat) findViewById(R.id.linear_layout_login);
        progressBar = (ProgressBar) findViewById(R.id.login_progress_bar);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login(){
        EditText editTextUsername = (EditText) findViewById(R.id.text_login_username);
        EditText editTextPassword = (EditText) findViewById(R.id.text_login_password);

        String username = editTextUsername.getText().toString();
        String password = editTextPassword.getText().toString();

        if (!username.equals("") && !password.equals("")){ // TODO: Additional user/password validations and error messages
            progressBar.setVisibility(View.VISIBLE);
            NetworkingHandler networkingHandler = new NetworkingHandler(this);
            networkingHandler.merchantLogin(username, password);
        } else {
            Snackbar.make(linearLayoutCompat, R.string.login_error_no_username_password, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        progressBar.setVisibility(View.GONE);
        String securityToken = "";
        try {
            securityToken = response.getString("securityToken");
        } catch(JSONException jsonException) {
            Log.d(this.getClass().getSimpleName(), "Error processing the request");
        }

        Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra(RegisterActivity.EXTRA_SECURITY_TOKEN, securityToken);
        startActivity(intent);
        Log.d(this.getClass().getSimpleName(), response.toString());
    }

    @Override
    public void onErrorResponse(String error) {
        progressBar.setVisibility(View.GONE);
        Snackbar.make(linearLayoutCompat, error, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        networkingHandler.cancel();
    }
}
