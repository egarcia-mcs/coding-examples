package com.example.challenge.weatherapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ForecastActivity extends AppCompatActivity implements LocationListener, ImageDownloaderTask.ImageDownloaderInterface {

    private TextView cityTextView;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        setContentView(R.layout.activity_forecast);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        refreshBackground();
        cityTextView = (TextView) findViewById(R.id.text_location);
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);
            // Meanwhile the precise location gets determined
            Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
            updateLocation(lastKnownLocation);
        } catch (SecurityException securityException){
            Log.d(this.getClass().getSimpleName(), "User denied the location permission, can't determine his location");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void updateLocation(Location location){
        String cityName = null;
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = gcd.getFromLocation(location.getLatitude(),
                    location.getLongitude(), 1);
            if (addresses.size() > 0) {
                System.out.println(addresses.get(0).getLocality());
                cityName = addresses.get(0).getLocality();
                cityTextView.setText(cityName);
            }
        }
        catch (IOException e) {
            // Couldn't  retrieve the city name
            Log.d(this.getClass().getSimpleName(), "Couldn't determine the city ");
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
        locationManager.removeUpdates(this); // We don't need constant location updates
        Log.d(this.getClass().getSimpleName(), "The user location is: Latitude " + location.getLatitude() + " Longitude "+ location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void refreshBackground() {
        ImageDownloaderTask imageDownloaderTask = new ImageDownloaderTask(this);
        try {
            imageDownloaderTask.execute("www.google.com");
        } catch (Exception ie){
            Log.d(this.getClass().getSimpleName(), ie.getMessage());
        }
    }

    @Override
    public void onResult(Bitmap bitmap) {
        Log.d(this.getClass().getSimpleName(), "Got a bitmap from the AsyncTask: " + bitmap);
    }
}
