package com.example.challenge.weatherapp;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;

public class ImageDownloaderTask  extends AsyncTask<String,Void,Bitmap> {

    public interface ImageDownloaderInterface {
        void onResult(Bitmap bitmap);
    }

    private ImageDownloaderInterface mDelegate;

    public ImageDownloaderTask(ImageDownloaderInterface delegate) {
        mDelegate = delegate;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ie){
            // Handle exception
            Log.d(this.getClass().getSimpleName(), ie.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        mDelegate.onResult(bitmap);
    }
}
