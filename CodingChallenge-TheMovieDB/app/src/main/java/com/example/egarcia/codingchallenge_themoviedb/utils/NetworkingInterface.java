package com.example.egarcia.codingchallenge_themoviedb.utils;

import org.json.JSONObject;

public interface NetworkingInterface {
    void onResult(JSONObject response);
    void onFailure(String errorMessage);
}
