package com.example.samples.programmingchallenges;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void problem_1_isCorrect() throws Exception {
        int[] test_1 = {1,2,3};
        assertEquals(4, Utils.count(test_1, 4));

        int[] test_2 = {-1,2,3};
        assertEquals(5, Utils.count(test_2, 4));
    }

    @Test
    public void problem_2_isCorrect() throws Exception {
        String stringA = "abc";
        String stringB = "def";
        assertEquals("adbecf", Utils.mergeStrings(stringA, stringB));
    }

    @Test
    public void problem_3_isCorrect() throws Exception {
        String stringCategories = "Baby,Gift Cards,Sporting Goods,Photo Shop,Deli,Baking Goods,International";
        String source = "Baby,Gift Cards,Sporting Goods,Photo Shop,Deli,Baking Goods,International\n" +
                "30\n" +
                "4816683327672908,39385271820845634478,Gift Cards,17.72,6.61\n" +
                "0911255208698955,25867248437110601432,Baby,12.83,2.75\n" +
                "6683414238680399,96520119728644480082,Appliances,17.34,3.13\n" +
                "4975828930086965,51531868891772291589,Automotive,12.27,1.86\n" +
                "0638901151099874,92685425904997978881,Photo Shop,17.22,4.21\n" +
                "6537712849594337,87908337056340759366,Bakery,10.73,0.85\n" +
                "3663405873668696,88620249668379048840,International,11.75,4.91\n" +
                "5616312143448106,26761211143034934597,Jewelry,18.77,8.48\n" +
                "7906242730279690,48996916239520647316,Deli,16.47,4.27\n" +
                "1276448878160824,67590620582640523769,Baking Goods,19.35,7.24\n" +
                "5234981004920030,97733090804595168574,Sporting Goods,16.89,0.12\n" +
                "1223651103809322,72582199553754045279,Pet Care,18.55,2.54\n" +
                "8008535506678390,98062313381021057830,Promotions,12.02,2.79\n" +
                "1057043898671327,21806588531783652237,Paper &amp; Plastics,16.94,0.11\n" +
                "1061696831507661,21452161144459387512,Travel &amp; Luggage,15.07,5.06\n" +
                "2582214516989584,76314716698150308390,K";
        Map<String, Object> coupon1 = readCoupon("4816683327672908,39385271820845634478,Gift Cards,17.72,6.61");
        Map<String, Object> coupon2 = readCoupon("0911255208698955,25867248437110601432,Baby,12.83,2.75");
        List<Map<String, Object>> coupons = new ArrayList<>(10);
        coupons.add(coupon1);
        coupons.add(coupon2);

        List<String> categories = new ArrayList<>(Arrays.asList(stringCategories.split(",")));

        List<Map<String, Object>> personalizedCoupons = Utils.personalizeCoupons(coupons, categories);
        personalizedCoupons.toString();
        //assertEquals("adbecf", Utils.mergeStrings(stringA, stringB));
    }

    @Test
    public void problem_4_is_correct() throws Exception {
        String[] bannedPrefixes = {"1034", "5", "993934", "33", "9"};
        String[] cardsToValidate = {"2552086989552589", "6724843711060148", "9758289300869651"};
        Utils.validateCards(bannedPrefixes, cardsToValidate);
        assertEquals(2,2);
    }

    private static Map<String, Object> readCoupon(String input) {
        String[] couponItems = input.split(",");
        Map<String,Object> coupon = new HashMap<>();
        coupon.put("upc", couponItems[0]);
        coupon.put("code", couponItems[1]);
        coupon.put("category", couponItems[2]);
        coupon.put("itemPrice", Float.parseFloat(couponItems[3]));
        coupon.put("couponAmount", Float.parseFloat(couponItems[4]));
        return coupon;
    }
}