package com.example.samples.programmingchallenges;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Utils {

    /** Unverified: Subarray Products **/
    static long count(int[] numbers, int k) {

        if(numbers == null) {
            throw new IllegalArgumentException("Invalid input");
        }

        long count = 0;
        long product;

        for(int currentIndex = 0; currentIndex < numbers.length; currentIndex++){
            product = numbers[currentIndex];

            if(product < k) {
                count ++;
            }

            for (int index = currentIndex + 1; index < numbers.length; index ++) {
                product *= numbers[index];
                if(product < k) {
                    count ++;
                }
            }
        }
        return count;
    }

    /** Verified Problem: Merge Strings **/
    static String mergeStrings(String a, String b) {
        StringBuilder stringBuilder = new StringBuilder("");
        int maxStringLength = (a.length() > b.length()) ? a.length() : b.length();

        for(int index = 0; index < maxStringLength; index ++) {
            if (index < a.length()){
                stringBuilder.append(a.charAt(index));
            }
            if (index < b.length()){
                stringBuilder.append(b.charAt(index));
            }
        }
        return stringBuilder.toString();
    }

    /** Unverified Problem: Personalized Products **/
    static List<Map<String, Object>> personalizeCoupons(List<Map<String, Object>> coupons,
                                                        List<String> preferredCategories) {
        String couponSensitiveField = "code";
        int maxResults = 10;

        List<Map<String, Object>> personalizedCoupons;

        // Filter coupons in the list
        personalizedCoupons = filterCoupons(coupons, preferredCategories);
        // Sort coupons
        personalizedCoupons = sortCoupons(personalizedCoupons);
        // Keep first 10
        personalizedCoupons.subList(0,maxResults);
        // Remove code field
        personalizedCoupons = removeField(personalizedCoupons, couponSensitiveField);
        // Return the results
        return personalizedCoupons;
    }

    private static List<Map<String, Object>> filterCoupons(List<Map<String, Object>> coupons, List<String> preferredCategories) {
        Iterator<Map<String, Object>> couponsIterator = coupons.iterator();
        String couponCategory;
        Map<String,Object> currentCoupon;

        while(couponsIterator.hasNext()) {

            currentCoupon = couponsIterator.next();
            // Remove coupons without a category
            if (!currentCoupon.containsKey("category")) {
                couponsIterator.remove();
                continue;
            }

            // Remove coupons whose category is not on the preferred categories
            couponCategory = (String) currentCoupon.get("category");
            if(!preferredCategories.contains(couponCategory)) {
                couponsIterator.remove();
            }
        }
        return coupons;
    }

    private static List<Map<String, Object>> sortCoupons(List<Map<String, Object>> coupons) {
        // Order using collections sort
        Collections.sort(coupons, new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                if ((Float)o2.get("couponAmount") / (Float)o2.get("itemPrice") > (Float)o1.get("couponAmount") / (Float)o1.get("itemPrice")) {
                    return 1;
                } else if ((Float)o2.get("couponAmount") / (Float)o2.get("itemPrice") < (Float)o1.get("couponAmount") / (Float)o1.get("itemPrice")) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        return coupons;
    }

    private static List<Map<String, Object>> removeField(List<Map<String, Object>> coupons, String field){
        Iterator<Map<String, Object>> couponsIterator = coupons.iterator();
        while(couponsIterator.hasNext()){
            couponsIterator.next().remove(field);
        }
        return coupons;
    }

    /** Verified Problem: Missing Words **/
    static String[] missingWords(String s, String t) {
        ArrayList<String> output = new ArrayList<>(s.length());
        String separator = " ";
        // Split the source String into a String array
        String[] sArray = s.split(separator);
        String[] tArray = t.split(separator);
        // For every word in the String array check if it is contained in t
        int  currentTWordIndex = 0;
        for (int sWordIndex = 0; sWordIndex < sArray.length; sWordIndex ++) {
            if (currentTWordIndex < tArray.length) {
                // If the word is contained in T at the current index
                if (sArray[sWordIndex].equals(tArray[currentTWordIndex])) {
                    currentTWordIndex ++;
                    continue;
                }
            }
            output.add(sArray[sWordIndex]);
        }
        return output.toArray(new String[0]);
    }

    /** Verified Problem: Validate Cards **/
    public static List<Map<String, Object>> validateCards(String[] bannedPrefixes, String[] cardsToValidate) {

        List<Map<String, Object>> result = new ArrayList<>();
        HashMap<String, Object> cardMap;
        boolean isValid = false, isAllowed = true;

        int numbers[];
        String card, subCard, nBanned, subBanned;
        int sum = 0, digit = 0;

        // For every card to be validated
        for(int i = 0; i < cardsToValidate.length; i++) {
            cardMap = new HashMap<>(3);
            card = cardsToValidate[i];
            subCard = card.substring(0, card.length()-1);
            numbers = new int[subCard.length()];

            // Validate the card according to the rules
            for (int j = 0; j < numbers.length; j++) {
                numbers[j] = Integer.parseInt("" + subCard.charAt(j)) *  2;
                sum = sum + numbers[j];
            }
            digit = sum % 10;
            if (digit == Integer.parseInt("" + card.charAt(card.length() - 1))){
                isValid = true;
            }

            for (int k = 0; k < bannedPrefixes.length; k++) {
                nBanned = bannedPrefixes[k];
                subBanned = card.substring(0, nBanned.length());
                if(subBanned.equals(nBanned)) {
                    isAllowed = false;
                }
            }
            // Put the card properties inside a map
            cardMap.put("card", card);
            cardMap.put("isValid", isValid);
            cardMap.put("isAllowed", isAllowed);
            result.add(cardMap);

            // Reset the flags
            isValid = false;
            isAllowed = true;
            sum = 0;
        }
        return result;
    }

}

